API automation testing with Rest Assured

Knowledge Java basic, RestAPI, TestNG and Rest Assured. Apply that knowledge to writing Automation test API.

The main task are:

- Create mavenproject, configuration porn with library TestNG, Rest Assured, hamcrest, jackson-databind.
- Build POM architecture and analysis api
- Write testcase api.
- Report extraction with ReportNG.

Deploy on local, clone project:
With https
git clone https://gitlab.com/thiennmfx22887/apiautomationtestingwith_restassured.git
With ssh
git clone git@gitlab.com:thiennmfx22887/apiautomationtestingwith_restassured.git

Must Skills knowledge
Java basic, oop, maven and POM.
know how to use TestNG, Rest Assured, hamcrest, jackson-databind.

Software: Eclipse IDE
