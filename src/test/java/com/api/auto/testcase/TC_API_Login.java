package com.api.auto.testcase;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.api.auto.utils.ExcelFileUtils;
import com.api.auto.utils.PropertiesFileUtils;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class TC_API_Login {
	private String account;
	private String password;
	private Response response;
	private ResponseBody responseBody;
	private JsonPath jsonBody;
	
	private ExcelFileUtils excelFileUtils;
	
//	đường dẫn đến file properties
	private static String CONFIG_PATH ="./configuration/configs.properties";
//	private static String TOKEN_PATH ="./configuration/token.properties";
	private static String TOKEN_PATH_EXCEL ="./configuration/token.xlsx";
	
	@BeforeClass
	public void init() throws Exception {
		excelFileUtils = new ExcelFileUtils();
		
		String baseUrl = PropertiesFileUtils.getProperty("baseurl", CONFIG_PATH);	
		String loginPath = PropertiesFileUtils.getProperty("loginPath", CONFIG_PATH);
		
		account = PropertiesFileUtils.getProperty("account", CONFIG_PATH);
		password = PropertiesFileUtils.getProperty("password", CONFIG_PATH);
		
		// make body
		Map<String, Object> accounts = new HashMap<String, Object>();
		accounts.put("account", account);
		accounts.put("password", password);

		RestAssured.baseURI = baseUrl;

		RequestSpecification req = RestAssured.given()
				.contentType(ContentType.JSON)
				.when()
				.body(accounts);
		
		response = req.post(loginPath);
		responseBody = response.getBody();
		jsonBody = responseBody.jsonPath();

		System.out.println(" " + responseBody.asPrettyString());

	}
	
	@Test(priority = 0)
	public void TC01_Validate200Ok() {
		assertEquals(200, response.getStatusCode(), "Status Check Failed!");    
	}
	
	@Test(priority = 1)
	public void TC02_ValidateMessage() {
		String expectMessage = "Đăng nhập thành công";
		
//		Check có trường message 
		assertTrue(responseBody.asString().contains("message"), "Message Check Failed!"); 
		
//		check nội dung Message 
		assertEquals(expectMessage, jsonBody.getString("message"), "Message notification Check Failed!"); 
	}

	@Test(priority = 2)
	public void TC03_ValidateToken() throws Exception {
//        kiểm chứng response body có chứa trường token hay không
		assertTrue(responseBody.asString().contains("token"), "token Check Failed!");
		
		String token = jsonBody.getString("token");
		
//		 lưu lại token.xlsx
		excelFileUtils.setTokenExcel(TOKEN_PATH_EXCEL, "Token" , 0, "Token", token);
		
//		Lưu token vào token.properties 
//		PropertiesFileUtils.setProperty("token", token, TOKEN_PATH);
	}
	
	@Test(priority = 3)
	public void TC04_ValidateUserType() {
         // kiểm chứng response body có chứa thông tin user và trường type hay không
		assertTrue(response.asString().contains("user"), "user Check Failed!");
		assertTrue(response.asString().contains("type"), "user.type Check Failed!");
		
		// kiểm chứng trường type có phải là “UNGVIEN”
		String expectType = "UNGVIEN";
		assertEquals(expectType, jsonBody.getString("user.type"), "type notification Check Failed!"); 
	}

	@Test(priority = 4)
	public void TC05_ValidateAccount() {
        // kiểm chứng response chứa thông tin user và trường account hay không
		assertTrue(responseBody.asString().contains("user"), "user Check Failed!");
		assertTrue(responseBody.asString().contains("account"), "user.account Check Failed!");
		
         // Kiểm chứng trường account có khớp với account đăng nhập
		assertEquals(account,jsonBody.getString("user.account"), "account not match!"); 
		
          // kiểm chứng response chứa thông tin user và trường password hay không
		assertTrue(responseBody.asString().contains("password"), "user.password Check Failed!");
		
          // Kiểm chứng trường password có khớp với password đăng nhập
		assertEquals(password,jsonBody.getString("user.password"), "password not match!"); 
	}

}
