package com.api.auto.testcase;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.api.auto.utils.ExcelFileUtils;
import com.api.auto.utils.PropertiesFileUtils;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class TC_API_CreateWork {
	private ExcelFileUtils excelFileUtils;
	private String token;
	private Response response;
	private ResponseBody responseBody;
	private JsonPath jsonBody;
	
//	đường dẫn đến file properties
	private static String CONFIG_PATH ="./configuration/configs.properties";
//	private static String TOKEN_PATH ="./configuration/token.properties";
	private static String TOKEN_PATH_EXCEL ="./configuration/token.xlsx";
	
//	Value cần thêm vào body
	String myWork  = "Tester";	
	String myExperience  = "2 năm";
	String myEducation  = "Đại học";
		
	@BeforeClass
	public void init() throws Exception {
//		Khởi tạo excel lấy ra token
		excelFileUtils = new ExcelFileUtils();
		token  = excelFileUtils.getTokenExcel(TOKEN_PATH_EXCEL, "Token", 0);
		
//		Lấy ra endpoint từ file properties
		String baseUrl   = PropertiesFileUtils.getProperty("baseurl", CONFIG_PATH);	
		String createWorkPath  = PropertiesFileUtils.getProperty("createWorkPath", CONFIG_PATH);
		
//		Truyền data vào body
		Map<String, Object> body = new HashMap<String, Object>();
		body.put("nameWork", myWork);
		body.put("experience", myExperience);
		body.put("education", myEducation);

//		Gán url
		RestAssured.baseURI = baseUrl;

//		Gửi repuest
		RequestSpecification req = RestAssured.given()
				.contentType(ContentType.JSON)
				.when()
				.header("token", token)
				.body(body);
		
		
		response = req.post(createWorkPath);
		responseBody = response.getBody();
		jsonBody = responseBody.jsonPath();

		System.out.println(" " + responseBody.asPrettyString());

	}
	
	@Test(priority = 0)
	public void TC01_Validate201Created() {
		assertEquals(201, response.getStatusCode(), "Status Check Failed!");    
	}
	
	@Test(priority = 1)
	public void TC02_ValidateWorkId() {		
//		Check có trường message 
		assertTrue(responseBody.asString().contains("id"), "id Check Failed!"); 
	}

	@Test(priority = 2)
	public void TC03_ValidateNameOfWorkMatched() {
//        kiểm chứng response body có chứa trường token hay không
		assertEquals(myWork, jsonBody.getString("nameWork"), "nameWork not match!"); 
	}
	
	@Test(priority = 3)
	public void TC03_ValidateExperienceMatched() {
		assertEquals(myExperience, jsonBody.getString("experience"), "myExperience not match!");     
	}

	@Test(priority = 4)
	public void TC03_ValidateEducationMatched() {
		assertEquals(myEducation, jsonBody.getString("education"), "myEducation not match!");     
	}
	
	@AfterClass
	public void afterTest() {
		RestAssured.baseURI = null; //Setup Base URI
		RestAssured.basePath = null;
	}
}
