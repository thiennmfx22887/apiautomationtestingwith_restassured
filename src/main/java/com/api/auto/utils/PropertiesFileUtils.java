package com.api.auto.utils;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertiesFileUtils {

//	Ghi file
	public static void setProperty(String key, String value, String filePath) {
		Properties properties= new Properties();
		FileOutputStream fileOutputStream = null;
		
		try {
			fileOutputStream = new FileOutputStream(filePath);
			properties.setProperty(key, value);
			properties.store(fileOutputStream, "Lưu giá trị mới");
			System.out.println("Lưu giá trị mới thành công");
		}catch(IOException ex) {
			ex.printStackTrace();
		}finally {
			//luôn nhảy vào đây dù có xảy ra exception hay không.
			//thực hiện đóng luồng ghi
			if (fileOutputStream != null) {
				try {
					fileOutputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
//	đọc file
	public static String getProperty(String key, String filePath) {
		Properties properties= new Properties();
		String value = null;
		FileInputStream fileInputStream = null;
		
		try {
			fileInputStream = new FileInputStream(filePath);
			properties.load(fileInputStream);
			value = properties.getProperty(key);
		}catch(Exception ex) {
			System.out.println("Không lấy được property");
			ex.printStackTrace();
		}finally {
//			luôn đóng luồng đọc sau khi đọc
			if(fileInputStream!=null) {
				try {
					fileInputStream.close();
				}catch(IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		return value;
		
	}
	
}
