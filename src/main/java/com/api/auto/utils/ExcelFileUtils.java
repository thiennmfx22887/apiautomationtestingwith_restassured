package com.api.auto.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelFileUtils {
	private FileInputStream fis;
	private FileOutputStream out;
	private XSSFWorkbook wb;
	private XSSFSheet sheet;
	private Row row;
	private Cell cell;
	
//	Ghi data
	public void setTokenExcel(String filePath, String sheetName, int numRow, String key, String value) throws Exception {
	
        wb = new XSSFWorkbook();
        sheet = wb.createSheet(sheetName);
        row = sheet.createRow(numRow);
        cell = row.createCell(numRow);
        cell.setCellValue(key);
        cell = row.createCell(numRow +1);
        cell.setCellValue(value);
        
        try {
			// ghi dữ liệu xuống file
			FileOutputStream out = new FileOutputStream(new File(filePath));
			wb.write(out);
			out.close();
			System.out.println("Token.xlsx written successfully on disk.");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
//	Đọc data
	public String getTokenExcel(String filePath, String sheetName, int numRow) throws Exception {
		FileInputStream fis = new FileInputStream(filePath);
        wb = (XSSFWorkbook) WorkbookFactory.create(fis);
        sheet = wb.getSheet(sheetName);
        row = sheet.getRow(numRow);
        cell = row.getCell(numRow + 1);
        String data = cell.getStringCellValue();
        
        return data;
	}

}
